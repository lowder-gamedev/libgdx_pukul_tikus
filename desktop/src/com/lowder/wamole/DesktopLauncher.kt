package com.lowder.wamole

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration

// Please note that on macOS your application needs to be started with the -XstartOnFirstThread JVM argument
object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = Lwjgl3ApplicationConfiguration().apply {
            setTitle("whack_a_mole")
            setForegroundFPS(144) // 60fps for normal
            useVsync(true)
            //setFullscreenMode(Lwjgl3ApplicationConfiguration.getDisplayMode())
            //setWindowedMode(1366, 768)

            val width = 768 * 0.7f
            val height = 1366 * 0.7f

            setWindowedMode(width.toInt(), height.toInt())
        }

        Lwjgl3Application(WhackAMole(), config)
    }
}