package com.lowder.wamole.anim

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.lowder.wamole.core.AssetManager
import com.lowder.wamole.core.DrawHelper

/**
 * Created by AntonyCS on 20/07/2024
 */
class MoleAnimationCollection(
    private val assetManager: AssetManager,
    val vector2: Vector2,
    val width: Float,
    val height: Float
) {
    private var animationStep: Int = 0

    fun resetAnimationStep() {
        animationStep = 0
    }

    fun progressAnimationStep() {
        animationStep++
    }

    fun getProgressStep() = animationStep

    fun drawIdleAnim(batch: SpriteBatch?) {
        DrawHelper.basicDrawBackground(
            batch,
            assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_EMPTY_HOLE),
            vector2,
            width,
            height
        )
    }

    fun drawPopupAnim(batch: SpriteBatch?) {
        val textureToRender = when (animationStep) {
            0 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_EMPTY_HOLE)
            1 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_POPUP_1)
            2 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_POPUP_2)
            else -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_POPUP_3)
        }
        DrawHelper.basicDrawBackground(
            batch,
            textureToRender,
            vector2,
            width,
            height
        )
    }

    fun drawPopdownAnim(batch: SpriteBatch?) {
        val textureToRender = when (animationStep) {
            0 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_POPUP_3)
            1 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_POPUP_2)
            2 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_POPUP_1)
            else -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_EMPTY_HOLE)
        }
        DrawHelper.basicDrawBackground(
            batch,
            textureToRender,
            vector2,
            width,
            height
        )
    }

    fun drawHitAnim(batch: SpriteBatch?) {
        val textureToRender = when (animationStep) {
            0 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_HIT_1)
            1 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_HIT_2)
            2 -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_HIT_3)
            else -> assetManager.getTexture(AssetManager.TEX_MOLE_ANIM_EMPTY_HOLE)
        }
        DrawHelper.basicDrawBackground(
            batch,
            textureToRender,
            vector2,
            width,
            height
        )
    }
}