package com.lowder.wamole.hud

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.lowder.wamole.core.GlobalConfig

/**
 * Created by AntonyCS on 21/07/2024
 */
class UserHUD {

    private var score = 0
    var font: BitmapFont = BitmapFont()
    val layout = GlyphLayout()

    init {
        font.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.MipMapLinearNearest)
        font.data.scale(5f)
        layout.setText(font, "score: 0")
    }

    fun addScore(addScore: Int) {
        score += addScore
        layout.setText(font, "score: $score")
    }

    fun draw(batch: SpriteBatch?) {
        font.draw(batch, layout, 50f, GlobalConfig.resolutionHeight - 50f)
    }
}