package com.lowder.wamole.stage

import com.badlogic.gdx.graphics.g2d.SpriteBatch

/**
 * Created by AntonyCS on 09/01/2024
 */
abstract class StageCore {
    abstract val stageId: Int
    abstract val batch: SpriteBatch?
    abstract val navigationCallback: StageNavigationCallback

    abstract fun create()
    abstract fun update()
    open fun collisionCheck() { }
    abstract fun render()
    abstract fun dispose()

    interface StageNavigationCallback {
        fun navigate(stageId: Int)
    }
}