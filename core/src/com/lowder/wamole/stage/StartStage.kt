package com.lowder.wamole.stage

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.lowder.wamole.core.AssetManager
import com.lowder.wamole.core.DrawHelper.basicDrawBackground
import com.lowder.wamole.core.GlobalConfig
import com.lowder.wamole.core.ext.toVector2
import com.lowder.wamole.enemy.Mole
import com.lowder.wamole.enemy.pool.MolePooling
import com.lowder.wamole.hud.UserHUD
import kotlin.system.measureTimeMillis

/**
 * Created by AntonyCS on 09/01/2024
 */
class StartStage(
    override var batch: SpriteBatch?,
    override val navigationCallback: StageNavigationCallback
): StageCore() {
    override val stageId = ENDLESS_STAGE_ID

    private lateinit var assetManager: AssetManager
    private lateinit var molePooling: MolePooling
    private lateinit var userHUD: UserHUD

    override fun create() {
        val loadTime = measureTimeMillis {
            // Load Graphic, Textures and Sounds
            batch = SpriteBatch()
            assetManager = AssetManager()
            userHUD = UserHUD()

            // Init Object
            molePooling = MolePooling(assetManager)
            for (x in 0..2) {
                for (y in 0..3) {
                    val posX = (x * 360f) + 52f
                    val posY = (y * 250f) + 480f
                    molePooling.createEnemies(Vector2(posX, posY))
                }
            }

            GlobalConfig.camera.position.set(Vector2(GlobalConfig.resolutionWidth / 2, GlobalConfig.resolutionHeight / 2), 0f)
            GlobalConfig.camera.update()
        }
    }

    override fun update() {
        val deltaTime = Gdx.graphics.deltaTime

        var cursorVector = Vector2(0f, 0f);
        if (Gdx.input.justTouched()) {
            cursorVector = GlobalConfig.camera.unproject(Vector3(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)).toVector2()
        }

        molePooling.update(deltaTime)
        molePooling.moles.forEach {
            it.update { currentLifeCycle ->
                if (currentLifeCycle == Mole.POP_DOWN_LIFE_CYCLE) {
                    molePooling.activeMoleCount--
                }
            }
            it.hitCheck(cursorVector) {
                molePooling.activeMoleCount--
                userHUD.addScore(5)
            }
        }

        GlobalConfig.camera.update()
    }

    override fun collisionCheck() {

    }

    override fun render() {
        // Draw region
        batch?.projectionMatrix = GlobalConfig.camera.combined
        batch?.begin()
        basicDrawBackground(
            batch,
            assetManager.getTexture(AssetManager.TEX_BACKGROUND),
            Vector2(0f, 0f),
            GlobalConfig.resolutionWidth,
            GlobalConfig.resolutionHeight
        )

        molePooling.moles.forEach { it.draw(batch) }
        userHUD.draw(batch)
        batch?.end()
    }

    override fun dispose() {
        assetManager.dispose()
    }

    companion object {
        const val ENDLESS_STAGE_ID = 1
    }
}