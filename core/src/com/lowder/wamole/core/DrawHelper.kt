package com.lowder.wamole.core

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2

/**
 * Created by AntonyCS on 09/01/2024
 */
object DrawHelper {
    fun standardDraw(batch: SpriteBatch?, texture: Texture, vector2: Vector2, rotationAngle: Float) {
        batch?.draw(
            /* texture = */ texture,
            /* x = */ vector2.x,
            /* y = */ vector2.y,
            /* originX = */ (texture.width / 2).toFloat(),
            /* originY = */ (texture.height / 2).toFloat(),
            /* width = */ texture.width.toFloat(),
            /* height = */ texture.height.toFloat(),
            /* scaleX = */ 1f,
            /* scaleY = */ 1f,
            /* rotation = */ rotationAngle,
            /* srcX = */ 0,
            /* srcY = */ 0,
            /* srcWidth = */ texture.width,
            /* srcHeight = */ texture.height,
            /* flipX = */ false,
            /* flipY = */ false
        )
    }

    fun basicDraw(
        batch: SpriteBatch?,
        texture: Texture,
        vector2: Vector2,
        scale: Float = 1f,
        flipX: Boolean = false,
        flipY: Boolean = false
    ) {
        batch?.draw(
            /* texture = */ texture,
            /* x = */ vector2.x,
            /* y = */ vector2.y,
            /* originX = */ 0f,
            /* originY = */ 0f,
            /* width = */ texture.width.toFloat() * scale,
            /* height = */ texture.height.toFloat() * scale,
            /* scaleX = */ 1f,
            /* scaleY = */ 1f,
            /* rotation = */ 0f,
            /* srcX = */ 0,
            /* srcY = */ 0,
            /* srcWidth = */ texture.width,
            /* srcHeight = */ texture.height,
            /* flipX = */ flipX,
            /* flipY = */ flipY
        )
    }

    fun basicAnimDraw(
        batch: SpriteBatch?,
        texture: Texture,
        vector2: Vector2,
        srcVector2: Vector2,
        srcWidth: Int,
        srcHeight: Int
    ) {
        batch?.draw(
            /* texture = */ texture,
            /* x = */ vector2.x,
            /* y = */ vector2.y,
            /* originX = */ 0f,
            /* originY = */ 0f,
            /* width = */ srcWidth.toFloat(),
            /* height = */ srcHeight.toFloat(),
            /* scaleX = */ 1f,
            /* scaleY = */ 1f,
            /* rotation = */ 0f,
            /* srcX = */ srcVector2.x.toInt(),
            /* srcY = */ srcVector2.y.toInt(),
            /* srcWidth = */ srcWidth,
            /* srcHeight = */ srcHeight,
            /* flipX = */ false,
            /* flipY = */ false
        )
    }

    fun basicDrawCustomSourceCenter(
        batch: SpriteBatch?,
        texture: Texture,
        vector2: Vector2,
        vector2Src: Vector2,
        width: Float,
        height: Float,
        scale: Float = 1f,
        rotation: Float = 0f,
        flipX: Boolean = false,
        flipY: Boolean = false
    ) {
        batch?.draw(
            /* texture = */ texture,
            /* x = */ (vector2.x - width / 2),
            /* y = */ (vector2.y - height / 2),
            /* originX = */ width / 2,
            /* originY = */ height / 2,
            /* width = */ width,
            /* height = */ height,
            /* scaleX = */ scale,
            /* scaleY = */ scale,
            /* rotation = */ rotation,
            /* srcX = */ vector2Src.x.toInt(),
            /* srcY = */ vector2Src.y.toInt(),
            /* srcWidth = */ width.toInt(),
            /* srcHeight = */ height.toInt(),
            /* flipX = */ flipX,
            /* flipY = */ flipY)
    }

    fun basicDrawBackground(
        batch: SpriteBatch?,
        texture: Texture,
        vector2: Vector2,
        width: Float,
        height: Float,
    ) {
        batch?.draw(texture, vector2.x, vector2.y, width, height)
    }

    fun debugDraw(batch: SpriteBatch?, texture: Texture, vector2: Vector2, width: Float, height: Float) {
        batch?.draw(texture, (vector2.x - width / 2), (vector2.y - height / 2), width, height)
    }
}