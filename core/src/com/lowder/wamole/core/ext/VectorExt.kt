package com.lowder.wamole.core.ext

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3

/**
 * Created by AntonyCS on 10/01/2024
 */
fun Vector3.toVector2() = Vector2(this.x, this.y)