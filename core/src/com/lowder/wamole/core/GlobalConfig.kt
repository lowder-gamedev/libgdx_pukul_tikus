package com.lowder.wamole.core

import com.badlogic.gdx.graphics.OrthographicCamera

/**
 * Created by AntonyCS on 09/01/2024
 */
object GlobalConfig {
    /*
    var resolutionWidth = 1980f
    var resolutionHeight = 1080f
     */
    var resolutionWidth = 1080f
    var resolutionHeight = 1980f
    var camera: OrthographicCamera = OrthographicCamera(resolutionWidth, resolutionHeight).apply {
        zoom = 1f // max 1.2f
    }
    var soundVolume = 0.5f
}