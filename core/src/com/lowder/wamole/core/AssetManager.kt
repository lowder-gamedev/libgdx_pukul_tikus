package com.lowder.wamole.core

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Texture

/**
 * Created by AntonyCS on 09/01/2024
 */
class AssetManager {
    var textures: MutableMap<String, Texture> = mutableMapOf()
    var sounds: MutableMap<String, Sound> = mutableMapOf()

    init {
        loadTextures()
        loadSounds()
    }

    fun getTexture(textureId: String) =
        textures[textureId] ?:
        throw NoSuchElementException("this textureId \"${textureId}\" is not loaded yet or doesn't exist")

    fun getTexture(texturePair: Pair<String, String>) =
        textures[texturePair.first] ?:
        throw NoSuchElementException("this texture \"${texturePair.second}\" is not loaded yet")

    fun playSound(soundId: String, pitch: Float = 1f, volumeMultiplier: Float = 1f) =
        sounds[soundId]?.play(GlobalConfig.soundVolume * volumeMultiplier, pitch, 1f) ?:
        throw NoSuchElementException("this soundId \"${soundId}\" is not loaded yet or doesn't exist")

    fun playSound(soundPair: Pair<String, String>, pitch: Float = 1f, volumeMultiplier: Float = 1f) =
        sounds[soundPair.first]?.play(GlobalConfig.soundVolume * volumeMultiplier, pitch, 1f) ?:
        throw NoSuchElementException("this sound \"${soundPair.second}\" is not loaded yet")

    private fun loadTextures() {
        loadToList(TEX_MOLE_SPRITE_TILE)
        loadToList(TEX_BACKGROUND)
        loadToList(TEX_MOLE_ANIM_EMPTY_HOLE)
        loadToList(TEX_MOLE_ANIM_HIT_1)
        loadToList(TEX_MOLE_ANIM_HIT_2)
        loadToList(TEX_MOLE_ANIM_HIT_3)
        loadToList(TEX_MOLE_ANIM_POPUP_1)
        loadToList(TEX_MOLE_ANIM_POPUP_2)
        loadToList(TEX_MOLE_ANIM_POPUP_3)
    }

    private fun loadSounds() {
        // Insert
    }

    private fun loadToList(texturePair: Pair<String, String>) {
        textures[texturePair.first] = Texture(texturePair.second).apply {
            setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
            anisotropicFilter = 16f
        }
    }

    private fun loadSoundToList(soundPair: Pair<String, String>) {
        sounds[soundPair.first] = Gdx.audio.newSound(Gdx.files.internal(soundPair.second))
    }

    fun dispose() {
        textures.forEach { it.value.dispose() }
        sounds.forEach { it.value.dispose() }
        textures.clear()
        sounds.clear()
    }

    companion object {
        val TEX_MOLE_SPRITE_TILE = "MOLE_SH" to "enemy/mole_spritesheet.png"
        val TEX_BACKGROUND = "BACKGROUND_BG" to "background/background_backyard.png"

        val TEX_MOLE_ANIM_EMPTY_HOLE = "MOLE_ANIM_HOLE" to "enemy/anim/mole_empty_hole.png"
        val TEX_MOLE_ANIM_HIT_1 = "MOLE_ANIM_HIT_1" to "enemy/anim/mole_hitted_1.png"
        val TEX_MOLE_ANIM_HIT_2 = "MOLE_ANIM_HIT_2" to "enemy/anim/mole_hitted_2.png"
        val TEX_MOLE_ANIM_HIT_3 = "MOLE_ANIM_HIT_3" to "enemy/anim/mole_hitted_3.png"
        val TEX_MOLE_ANIM_POPUP_1 = "MOLE_ANIM_POP_1" to "enemy/anim/mole_popup_1.png"
        val TEX_MOLE_ANIM_POPUP_2 = "MOLE_ANIM_POP_2" to "enemy/anim/mole_popup_2.png"
        val TEX_MOLE_ANIM_POPUP_3 = "MOLE_ANIM_POP_3" to "enemy/anim/mole_popup_3.png"
    }
}