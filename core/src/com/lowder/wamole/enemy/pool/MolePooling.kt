package com.lowder.wamole.enemy.pool

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.lowder.wamole.core.AssetManager
import com.lowder.wamole.enemy.Mole

/**
 * Created by AntonyCS on 10/01/2024
 */
class MolePooling(
    private val assetManager: AssetManager
) {
    var moles: MutableList<Mole> = mutableListOf()
    var currentTime: Float = 0f
    var activeMoleCount = 0
    var activeMoleCountMax = 2

    fun createEnemies(spawnVector2: Vector2) {
        moles.add(Mole(assetManager, spawnVector2))
    }

    fun update(deltaTime: Float) {
        currentTime += deltaTime
        if (currentTime > 1f) {
            currentTime = 0f

            if (activeMoleCount < activeMoleCountMax) {
                popEnemies()
            }
        }
    }

    private fun popEnemies() {
        val moleIndex = MathUtils.random(0, (moles.count() - 1))
        moles[moleIndex].setLifecycle(Mole.POP_UP_LIFE_CYCLE)
        activeMoleCount++
    }
}