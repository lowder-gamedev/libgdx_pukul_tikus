package com.lowder.wamole.enemy

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.lowder.wamole.anim.MoleAnimationCollection
import com.lowder.wamole.core.AssetManager

/**
 * Created by AntonyCS on 10/01/2024
 */
class Mole(
    private val assetManager: AssetManager,
    var vector2: Vector2
) {
    // 0 = idle
    // 1 = popup
    // 2 = hit
    var currentLifeCycle: Int = 0
    var isHittable = false

    val srcWidth = 190f * 1.4f
    val srcHeight = 144f * 1.4f

    var currentAnimDT = 0f
    var maxAnimDT = 0.15f

    var currentRandomDT = 0f
    var maxRandomDT = 0f

    var rectangle: Rectangle = Rectangle(vector2.x, vector2.y, srcWidth, srcHeight)

    var moleAnimationCollection: MoleAnimationCollection

    init {
        maxRandomDT = MathUtils.random(0.5f, 1f)
        moleAnimationCollection = MoleAnimationCollection(assetManager, vector2, srcWidth, srcHeight)
    }

    private fun idle() {
        isHittable = false
        moleAnimationCollection.resetAnimationStep()
    }

    private fun popupRevamp() {
        onAnimationTime {
            moleAnimationCollection.progressAnimationStep()
            if (moleAnimationCollection.getProgressStep() >= 2) {
                isHittable = true
            }
        }

        if (moleAnimationCollection.getProgressStep() >= 3) {
            onPopdownTime {
                maxRandomDT = MathUtils.random(0.5f, 1f)
                maxAnimDT = MathUtils.random(0.05f, 0.15f)
                setLifecycle(POP_DOWN_LIFE_CYCLE)
            }
        }
    }

    private fun popDownRevamp(callbackEndLifeCycle: () -> Unit) {
        onAnimationTime {
            moleAnimationCollection.progressAnimationStep()
            if (moleAnimationCollection.getProgressStep() >= 3) {
                setLifecycle(IDLE_LIFE_CYCLE)
                callbackEndLifeCycle.invoke()
            } else if (moleAnimationCollection.getProgressStep() >= 1) {
                isHittable = false
            }
        }
    }

    private fun hitRevamp() {
        onAnimationTime {
            moleAnimationCollection.progressAnimationStep()
            if (moleAnimationCollection.getProgressStep() >= 3) {
                setLifecycle(IDLE_LIFE_CYCLE)
            } else if (moleAnimationCollection.getProgressStep() >= 2) {
                isHittable = false
            }
        }
    }

    fun update(callback: (Int) -> Unit) {
        when (currentLifeCycle) {
            IDLE_LIFE_CYCLE -> idle()
            POP_UP_LIFE_CYCLE -> popupRevamp()
            HIT_LIFE_CYCLE -> hitRevamp()
            POP_DOWN_LIFE_CYCLE -> popDownRevamp {
                callback.invoke(POP_DOWN_LIFE_CYCLE)
            }
        }
    }

    fun hitCheck(vector2: Vector2, callback: () -> Unit) {
        if (rectangle.contains(vector2) && (currentLifeCycle == 1 || currentLifeCycle == 3) && isHittable) {
            setLifecycle(HIT_LIFE_CYCLE)
            callback.invoke()
        }
    }

    fun setLifecycle(currentLifeCycle: Int) {
        this.currentLifeCycle = currentLifeCycle
        this.moleAnimationCollection.resetAnimationStep()
    }

    fun draw(batch: SpriteBatch?) {
        when (currentLifeCycle) {
            IDLE_LIFE_CYCLE -> moleAnimationCollection.drawIdleAnim(batch)
            POP_UP_LIFE_CYCLE -> moleAnimationCollection.drawPopupAnim(batch)
            HIT_LIFE_CYCLE -> moleAnimationCollection.drawHitAnim(batch)
            POP_DOWN_LIFE_CYCLE -> moleAnimationCollection.drawPopdownAnim(batch)
        }
    }

    private fun onAnimationTime(unit: () -> Unit) {
        currentAnimDT += Gdx.graphics.deltaTime
        if (currentAnimDT > maxAnimDT) {
            currentAnimDT = 0f
            unit.invoke()
        }
    }

    private fun onPopdownTime(unit: () -> Unit) {
        currentRandomDT += Gdx.graphics.deltaTime
        if (currentRandomDT > maxRandomDT) {
            currentRandomDT = 0f
            unit.invoke()
        }
    }

    companion object {
        const val IDLE_LIFE_CYCLE = 0
        const val POP_UP_LIFE_CYCLE = 1
        const val HIT_LIFE_CYCLE = 2
        const val POP_DOWN_LIFE_CYCLE = 3
    }
}