package com.lowder.wamole

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.lowder.wamole.stage.StageCore
import com.lowder.wamole.stage.StartStage

class WhackAMole : ApplicationAdapter(), StageCore.StageNavigationCallback {
    private var batch: SpriteBatch? = null
    var selectedStage: StageCore? = null
    var stageList: List<StageCore> = listOf()


    override fun navigate(stageId: Int) {
        selectedStage?.dispose()
        selectedStage = stageList.find { it.stageId == stageId }
        selectedStage?.create()
    }

    override fun create() {
        batch = SpriteBatch()
        stageList = listOf(
            StartStage(batch, this)
        )
        selectedStage = stageList.first()

        selectedStage?.create()
    }

    override fun render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT or if (Gdx.graphics.bufferFormat.coverageSampling) GL20.GL_COVERAGE_BUFFER_BIT_NV else 0)
        selectedStage?.collisionCheck()
        selectedStage?.update()
        selectedStage?.render()
    }

    override fun dispose() {
        batch?.dispose()
    }
}